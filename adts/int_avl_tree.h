#ifndef INT_AVL_TREE_H
#define INT_AVL_TREE_H

#define AVL_TREE_NAME int
#define AVL_TREE_TYPE int

#define AVL_TREE_SHOW_VALUE(f,v) fprintf(f, "%d", v)

#define AVL_TREE_VALUE_COMPARE(v1,v2) (v1 - v2)

#include "adts/templates/avl_tree.h"

#include "adts/glue/unglue_avl_tree.h"

#endif

