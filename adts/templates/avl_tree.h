
#ifndef AVL_TREE_NAME
   #error "must #define AVL_TREE_NAME before including " __FILE__
#endif

#ifndef AVL_TREE_TYPE
   #error "must #define AVL_TREE_TYPE before including " __FILE__
#endif


#include <stdio.h>
#include <stdlib.h> //for size_t

#include "adts/glue/glue.h"


#define AVL_TREE MAKE_GENERIC_NAME(AVL_TREE_NAME, avl_tree)
#define PROTOTYPE(name) MAKE_GENERIC_NAME(AVL_TREE, name)


typedef struct AVL_TREE* AVL_TREE;


AVL_TREE PROTOTYPE(create) ();
size_t PROTOTYPE(size) (AVL_TREE);
void PROTOTYPE(insert) (AVL_TREE, AVL_TREE_TYPE);
int PROTOTYPE(remove) (AVL_TREE, AVL_TREE_TYPE);
int PROTOTYPE(find) (AVL_TREE, AVL_TREE_TYPE);
void PROTOTYPE(show) (AVL_TREE);
void PROTOTYPE(fshow) (AVL_TREE, FILE*);
void PROTOTYPE(destroy) (AVL_TREE);

#undef AVL_TREE
#undef PROTOTYPE


