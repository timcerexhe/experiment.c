
#ifndef QUEUE_NAME
   #error "must #define QUEUE_NAME before including" __FILE__
#endif

#ifndef QUEUE_TYPE
   #error "must #define QUEUE_TYPE before including" __FILE__
#endif


#include <stdio.h>
#include <stdlib.h> //for size_t

#include "adts/glue/glue.h"


#define QUEUE MAKE_GENERIC_NAME(QUEUE_NAME, queue)
#define PROTOTYPE(name) MAKE_GENERIC_NAME(QUEUE, name)


typedef struct QUEUE* QUEUE;


QUEUE PROTOTYPE(create) ();
size_t PROTOTYPE(size) (QUEUE);
void PROTOTYPE(push) (QUEUE, QUEUE_TYPE);
QUEUE_TYPE PROTOTYPE(pop) (QUEUE);
QUEUE_TYPE PROTOTYPE(top) (QUEUE);
void PROTOTYPE(show) (QUEUE);
void PROTOTYPE(fshow) (QUEUE, FILE*);
void PROTOTYPE(destroy) (QUEUE);

#undef QUEUE
#undef PROTOTYPE


