
#ifndef VECTOR_NAME
   #error "must #define VECTOR_NAME before including" __FILE__
#endif

#ifndef VECTOR_TYPE
   #error "must #define VECTOR_TYPE before including" __FILE__
#endif


#include <stdio.h>
#include <stdlib.h> //for size_t

#include "adts/glue/glue.h"


#define VECTOR MAKE_GENERIC_NAME(VECTOR_NAME, vector)
#define PROTOTYPE(name) MAKE_GENERIC_NAME(VECTOR, name)


typedef struct VECTOR* VECTOR;


VECTOR PROTOTYPE(create) ();
size_t PROTOTYPE(size) (VECTOR);
void PROTOTYPE(push_back) (VECTOR, VECTOR_TYPE);
VECTOR_TYPE PROTOTYPE(remove) (VECTOR, size_t);
VECTOR_TYPE PROTOTYPE(at) (VECTOR, size_t);
void PROTOTYPE(show) (VECTOR);
void PROTOTYPE(fshow) (VECTOR, FILE*);
void PROTOTYPE(destroy) (VECTOR);

#undef VECTOR
#undef PROTOTYPE

