
#ifndef STACK_NAME
   #error "must #define STACK_NAME before including" __FILE__
#endif

#ifndef STACK_TYPE
   #error "must #define STACK_TYPE before including" __FILE__
#endif


#include <stdio.h>
#include <stdlib.h> //for size_t

#include "adts/glue/glue.h"


#define STACK MAKE_GENERIC_NAME(STACK_NAME, stack)
#define PROTOTYPE(name) MAKE_GENERIC_NAME(STACK, name)


typedef struct STACK* STACK;


STACK PROTOTYPE(create) ();
size_t PROTOTYPE(size) (STACK);
void PROTOTYPE(push) (STACK, STACK_TYPE);
STACK_TYPE PROTOTYPE(pop) (STACK);
STACK_TYPE PROTOTYPE(top) (STACK);
void PROTOTYPE(show) (STACK);
void PROTOTYPE(fshow) (STACK, FILE*);
void PROTOTYPE(destroy) (STACK);

#undef STACK
#undef PROTOTYPE

