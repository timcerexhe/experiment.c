
#ifndef STACK_ELEMENT_COPY
   #define STACK_ELEMENT_COPY(v) v //default: pass by value
#endif

#ifndef STACK_ELEMENT_FREE
   #define STACK_ELEMENT_FREE(v) //default: element wasn't malloc'd --> nothing to free
#endif


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "magic_util/memory.h"
#include "magic_util/error.h"
#include "adts/templates/stack.h"

#define STACK MAKE_GENERIC_NAME(STACK_NAME, stack)
#define PROTOTYPE(name) MAKE_GENERIC_NAME(STACK, name)


#define INITIAL_STACK_ALLOCATION 10
#define STACK_RESIZE_FACTOR 2
#define STACK_INCREASE_RATIO 0.75
#define STACK_DECREASE_RATIO 0.15



struct STACK {
   STACK_TYPE* cells;
   size_t end; //end = "1 passed last index" = where to insert (= 0 if stack is empty)
   size_t alloc_size;
};


static void PROTOTYPE(resize) (STACK, size_t);


STACK PROTOTYPE(create) () {
   ASSERT(INITIAL_STACK_ALLOCATION > 0, "initial allocation must be greater than zero in %s\n", __func__);
   STACK s = NULL;
   //printf("allocate %d bytes (not %d bytes!)\n", sizeof(*v), sizeof(v));
   s = MAGIC_ADT_MALLOC(sizeof(*s));
   s->alloc_size = INITIAL_STACK_ALLOCATION;
   s->cells = MAGIC_ADT_MALLOC(sizeof(STACK_TYPE) * s->alloc_size);
   s->end = 0;
   return s;
}

size_t PROTOTYPE(size) (STACK s) {
   return s->end;
}

void PROTOTYPE(push) (STACK s, STACK_TYPE val) {
   size_t size = PROTOTYPE(size) (s);

   if (size > s->alloc_size * STACK_INCREASE_RATIO) {
      size_t new_size = s->alloc_size * STACK_RESIZE_FACTOR;
      ASSERT (new_size > s->alloc_size, "error in stack resize parameters (push should increase capacity)");
      PROTOTYPE(resize) (s, new_size);
   }

   //add the element
   s->cells[s->end] = STACK_ELEMENT_COPY(val);

   //increase the pointer
   s->end += 1;
}

STACK_TYPE PROTOTYPE(pop) (STACK s) {
   ASSERT(s->end > 0, "cannot pop from empty stack");

   //remember what we pulled out
   STACK_TYPE rm = s->cells[s->end - 1];

   //decrease the pointer
   s->end -= 1;

   if (s->end < STACK_DECREASE_RATIO * s->alloc_size) {
      size_t new_size = s->alloc_size / STACK_RESIZE_FACTOR;
      PROTOTYPE(resize) (s, new_size);
   }

   return rm;
}

STACK_TYPE PROTOTYPE(top) (STACK s) {
   ASSERT(s->end > 0, "cannot check the top element of an empty stack");
   return s->cells[s->end - 1];
}

void PROTOTYPE(show) (STACK s) {
   PROTOTYPE(fshow) (s, stdout);
}

void PROTOTYPE(fshow) (STACK s, FILE* f) {
   fprintf(f, "< ");

   size_t i;
   for (i = 0; i != s->end; ++i) {
      fprintf(f, " ");
      STACK_SHOW_VALUE (f, s->cells[i]);
   }

   if (s->end == 0) { //empty
      fprintf(f, "empty stack");
   } else { //otherwise orient it!
      fprintf(f, " = top");
   }

   fprintf(f, " >");
   fflush(f);
}

void PROTOTYPE(destroy) (STACK s) {
   size_t i;
   for (i = 0; i != s->end; ++i) {
      STACK_ELEMENT_FREE(s->cells[i]);
   }

   MAGIC_ADT_FREE(s->cells);
   MAGIC_ADT_FREE(s);
}

static void PROTOTYPE(resize) (STACK s, size_t new_size) {
   if (new_size < INITIAL_STACK_ALLOCATION) {
      return; //punk kids trying to steal my house ...
   }

   //alloc the new array
   STACK_TYPE* new_cells = MAGIC_ADT_MALLOC(sizeof(STACK_TYPE) * new_size);

   //copy across
   unsigned long oldI = 0;
   unsigned long newI = 0;
   while (oldI != s->end) {
      new_cells[newI] = s->cells[oldI];
      ++newI;
      ++oldI;
   }

   s->end = newI;
   s->alloc_size = new_size;

   //free the old array
   MAGIC_ADT_FREE(s->cells);

   //and update the structure's array
   s->cells = new_cells;
}



