
#ifndef MAP_NAME
   #error "must #define MAP_NAME before including " __FILE__
#endif

#ifndef MAP_KEY_TYPE
   #error "must #define MAP_KEY_TYPE before including " __FILE__
#endif

#ifndef MAP_VALUE_TYPE
   #error "must #define MAP_VALUE_TYPE before including " __FILE__
#endif

#ifndef MAP_NOT_FOUND
   #error "must #define MAP_NOT_FOUND (empty value) before including " __FILE__
#endif


#include <stdio.h>
#include <stdlib.h>

#include "adts/glue/glue.h"


#define MAP MAKE_GENERIC_NAME(MAP_NAME, map)
#define PROTOTYPE(name) MAKE_GENERIC_NAME(MAP, name)


typedef struct MAP* MAP;


MAP PROTOTYPE(create) ();
unsigned long PROTOTYPE(size) (MAP);
int PROTOTYPE(has_key) (MAP, MAP_KEY_TYPE);
void PROTOTYPE(insert) (MAP, MAP_KEY_TYPE, MAP_VALUE_TYPE);
MAP_VALUE_TYPE PROTOTYPE(remove) (MAP, MAP_KEY_TYPE);
MAP_VALUE_TYPE PROTOTYPE(get) (MAP, MAP_KEY_TYPE);
void PROTOTYPE(show) (MAP);
void PROTOTYPE(fshow) (MAP m, FILE* f);
void PROTOTYPE(destroy) (MAP);


