
#ifndef MAP_NAME
   #error "must #define MAP_NAME before including " __FILE__
#endif

#ifndef MAP_KEY_TYPE
   #error "must #define MAP_KEY_TYPE before including " __FILE__
#endif

#ifndef MAP_VALUE_TYPE
   #error "must #define MAP_VALUE_TYPE before including " __FILE__
#endif

#ifndef MAP_NOT_FOUND
   #error "must #define MAP_NOT_FOUND (empty value) before including " __FILE__
#endif


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "magic_util/memory.h"
#include "magic_util/error.h"
#include "adts/templates/map.h"

#define MAP MAKE_GENERIC_NAME(MAP_NAME, map)
#define MAP_CELL MAKE_GENERIC_NAME(MAP_NAME, map_cell)
#define PROTOTYPE(name) MAKE_GENERIC_NAME(MAP, name)

#define TRAVERSER(fn) void (*fn)(MAP_KEY_TYPE, MAP_VALUE_TYPE, void*)


#define INITIAL_MAP_ALLOCATION 10
#define MAP_RESIZE_FACTOR 2
#define MAP_INCREASE_RATIO 0.75
#define MAP_DECREASE_RATIO 0.15


#ifndef MAP_KEY_COPY
   #define MAP_KEY_COPY(key) key //default: copy by value
#endif

#ifndef MAP_KEY_HASH
   #define MAP_KEY_HASH(key) ((unsigned long) key) //default: cast 'whatever' to int!
#endif

#ifndef MAP_KEY_EQUAL
   #define MAP_KEY_EQUAL(k1, k2) (k1 == k2) //default: compare by value
#endif

#ifndef MAP_KEY_FREE
   #define MAP_KEY_FREE(key) //default: do nothing (fine for non-pointer types)
#endif

#ifndef MAP_VALUE_COPY
   #define MAP_VALUE_COPY(value) value //default: copy by value
#endif

#ifndef MAP_VALUE_EQUAL
   #define MAP_VALUE_EQUAL(v1, v2) (v1 == v2) //default: compare by value
#endif

#ifndef MAP_VALUE_FREE
   #define MAP_VALUE_FREE(v) //default: do nothing
#endif


#if defined MAP_DEBUG_HASH_DISTRIBUTION && !defined MAP_DEBUG_HASH_FILE

   #ifndef MAP_DEBUG_HASH_FILE_NAME
      #define MAP_DEBUG_HASH_FILE_NAME "/tmp/hash.tmp"
   #endif

   #define MAP_DEBUG_HASH_FILE 1

   static int hash_update_should_log() {
      //return 0;
      static int update = 0;
      ++update;
      return ((update % 1000) == 1); //do it on 1 so there should always be a log!
   }

   static FILE* get_map_hash_file() {
      static FILE* f = NULL;
      if (f == NULL) {
         f = fopen(MAP_DEBUG_HASH_FILE_NAME, "a"); //append; rely on test framework to clean up!
      }
      return f;
   }

#endif



struct MAP_CELL {
   MAP_KEY_TYPE key;
   MAP_VALUE_TYPE value;
   struct MAP_CELL* next;
};

struct MAP {
   struct MAP_CELL** cells;
   unsigned long size;
   unsigned long alloc_size;
};



static void PROTOTYPE(resize) (MAP, unsigned long);

#ifdef MAP_DEBUG_HASH_DISTRIBUTION
   static void PROTOTYPE(log_hash_distribution)  (MAP m, const char* const msg, FILE* f);
#endif



MAP PROTOTYPE(create) () {
   ASSERT(INITIAL_MAP_ALLOCATION > 0, "initial allocation must be greater than zero in %s\n", __func__);
   MAP m = NULL;
   //printf("allocate %d bytes (not %d bytes!)\n", sizeof(*v), sizeof(v));

   m = (MAP) MAGIC_ADT_MALLOC(sizeof(*m));
   m->alloc_size = INITIAL_MAP_ALLOCATION;
   m->size = 0;
   m->cells = (struct MAP_CELL**) MAGIC_ADT_MALLOC(sizeof(struct MAP_CELL*) * m->alloc_size);

   unsigned long i;
   for (i = 0; i < m->alloc_size; ++i) {
      m->cells[i] = NULL;
   }

   return m;
}

unsigned long PROTOTYPE(size) (MAP m) {
   return m->size;
}

int PROTOTYPE(has_key) (MAP m, MAP_KEY_TYPE key) {
   MAP_VALUE_TYPE value = PROTOTYPE(get) (m, key);
   return !MAP_VALUE_EQUAL(value, MAP_NOT_FOUND);
}

void PROTOTYPE(insert) (MAP m, MAP_KEY_TYPE key, MAP_VALUE_TYPE value) {
   //if the map is half-full, then make it bigger (double it)
   if (m->size > MAP_INCREASE_RATIO * m->alloc_size) {
      PROTOTYPE(resize) (m, MAP_RESIZE_FACTOR * m->alloc_size);
   }

   //hash the key (and mod to make it fit the array)
   unsigned long hash = MAP_KEY_HASH(key) % m->alloc_size;

   //create the new cell and put it (at the head) of the linked list
   struct MAP_CELL* cell = (struct MAP_CELL*) MAGIC_ADT_MALLOC(sizeof(struct MAP_CELL));
   cell->key = MAP_KEY_COPY(key);
   cell->value = MAP_VALUE_COPY(value);
   cell->next = m->cells[hash];
   m->cells[hash] = cell;

   //update the size
   m->size += 1;

   #ifdef MAP_DEBUG_HASH_DISTRIBUTION
   if (hash_update_should_log()) {
      char msg[100];
      snprintf(msg, 100, "map %p insert (size %ld, alloc %ld)", m, m->size, m->alloc_size);
      PROTOTYPE(log_hash_distribution) (m, msg, get_map_hash_file());
   }
   #endif

}

MAP_VALUE_TYPE PROTOTYPE(remove) (MAP m, MAP_KEY_TYPE key) {
   unsigned long hash = MAP_KEY_HASH(key) % m->alloc_size;

   //traverse the linked list for the thing we are after
   struct MAP_CELL* previous = NULL;
   struct MAP_CELL* rm = m->cells[hash];
   while (rm != NULL && !MAP_KEY_EQUAL(rm->key, key)) {
      previous = rm;
      rm = rm->next;
   }

   MAP_VALUE_TYPE found = MAP_NOT_FOUND;

   //did we find it?
   if (rm != NULL) {

      //pull it out
      if (previous == NULL) { //it was the head
         m->cells[hash] = rm->next;
      } else {
         previous = rm->next;
      }

      //drop the size
      m->size -= 1;

      //remember what we pulled out and free the cell
      found = rm->value;
      MAP_KEY_FREE(rm->key);
      MAGIC_ADT_FREE(rm);

      //resize if the map is now really sparse (half it)
      if (m->size < MAP_DECREASE_RATIO * m->alloc_size && m->alloc_size > INITIAL_MAP_ALLOCATION) {
         PROTOTYPE(resize) (m, m->alloc_size / MAP_RESIZE_FACTOR);
      }
   }

   #ifdef MAP_DEBUG_HASH_DISTRIBUTION
   if (hash_update_should_log()) {
      char msg[100];
      snprintf(msg, 100, "map %p remove (size %ld, alloc %ld)", m, m->size, m->alloc_size);
      PROTOTYPE(log_hash_distribution) (m, msg, get_map_hash_file());
   }
   #endif

   return found;
}

MAP_VALUE_TYPE PROTOTYPE(get) (MAP m, MAP_KEY_TYPE key) {
   unsigned long hash = MAP_KEY_HASH(key) % m->alloc_size;
   struct MAP_CELL* c = m->cells[hash];
   while (c != NULL && !MAP_KEY_EQUAL(c->key, key)) {
      c = c->next;
   }

   MAP_VALUE_TYPE found = MAP_NOT_FOUND;
   if (c != NULL) {
      found = c->value;
   }

   return found;
}

void PROTOTYPE(show) (MAP m) {
   PROTOTYPE(fshow) (m, stdout);
}

void PROTOTYPE(fshow) (MAP m, FILE* f) {
   unsigned long i;
   fprintf(f, "{");
   for (i = 0; i < m->alloc_size; ++i) {
      struct MAP_CELL* c = m->cells[i];
      while (c != NULL) {
         fprintf(f, " ");
         MAP_SHOW_KEY(f, c->key);
         fprintf(f, ":");
         MAP_SHOW_VALUE(f, c->value);
         c = c->next;
      }
   }
   fprintf(f, " }");
   fflush(f);
}

void PROTOTYPE(traverse) (MAP m, TRAVERSER(fn), void* payload) {
   unsigned long i;
   for (i = 0; i < m->alloc_size; ++i) {
      struct MAP_CELL* c = m->cells[i];
      while (c != NULL) {
         fn(c->key, c->value, payload);
         c = c->next;
      }
   }
}

void PROTOTYPE(destroy) (MAP m) {
   unsigned long i;

   //for each index
   for (i = 0; i < m->alloc_size; ++i) {
      //free the whole linked list
      struct MAP_CELL* c = m->cells[i];
      struct MAP_CELL* temp;
      while (c != NULL) {
         temp = c->next;
         MAP_KEY_FREE(c->key);
         MAP_VALUE_FREE(c->value);
         MAGIC_ADT_FREE(c);
         c = temp;
      }
   }

   //plus the top-level junk
   MAGIC_ADT_FREE(m->cells);
   MAGIC_ADT_FREE(m);
}

static void PROTOTYPE(resize) (MAP m, unsigned long new_size) {
   unsigned long original_size = m->alloc_size;
//   printf("resizing from %ld to %ld\n", original_size, new_size);

/*   #ifdef MAP_DEBUG_HASH_DISTRIBUTION
   char pre_msg[100];
   snprintf(pre_msg, 100, "pre-map %p resize", m);
   PROTOTYPE(log_hash_distribution) (m, pre_msg, get_map_hash_file());
   #endif*/


   //alloc the new array
   struct MAP_CELL** new_cells = (struct MAP_CELL**) MAGIC_ADT_MALLOC(sizeof(struct MAP_CELL*) * new_size);
   ASSERT(new_cells != NULL, "out of space trying to resize map to %ld cells", new_size);

   unsigned long i;
   for (i = 0; i < new_size; ++i) {
      new_cells[i] = NULL;
   }

   //copy across (remember to rehash!)
   unsigned long hash;
   for (i = 0; i < original_size; ++i) {
      struct MAP_CELL* c = m->cells[i];
      struct MAP_CELL* next;
      while (c != NULL) {
         //generate the new hash
         hash = MAP_KEY_HASH(c->key) % new_size;

         //remember the next cell in the old list
         next = c->next;

         //move this cell to the new list
         c->next = new_cells[hash];
         new_cells[hash] = c;

         //move to the next thing in the old list
         c = next;
      }
   }

   //free the old array
   MAGIC_ADT_FREE(m->cells);

   //update the alloc size
   m->alloc_size = new_size;

   //and update the structure's array
   m->cells = new_cells;

   #ifdef MAP_DEBUG_HASH_DISTRIBUTION
   char post_msg[100];
   snprintf(post_msg, 100, "resize map %p from %ld to %ld", m, original_size, new_size);
   PROTOTYPE(log_hash_distribution) (m, post_msg, get_map_hash_file());
   #endif

}

#ifdef MAP_DEBUG_HASH_DISTRIBUTION

static void PROTOTYPE(log_hash_distribution)  (MAP m, const char* const msg, FILE* f) {
   fprintf(f, "%s |", msg);
   unsigned long i;
   unsigned long max = 0;
   unsigned long used = 0;
   unsigned long unused = 0;
   //unsigned long average_sum = 0;
   for (i = 0; i < m->alloc_size; ++i) {
      struct MAP_CELL* c = m->cells[i];
      unsigned long count = 0;
      while (c != NULL) {
         c = c->next;
         ++count;
      }
      fprintf(f, " %lu", count);

      if (count > max) {
         max = count;
      }
      if (count == 0) {
         ++unused;
      } else {
         ++used;
      }
      //average_sum += (count * 100 / m->alloc_size);
   }
   assert(used + unused == m->alloc_size);
   unsigned long avg = m->size * 100 / m->alloc_size;
   unsigned long use_avg = used * 100 / m->alloc_size;
   //unsigned long running_avg = average_sum * 100 / m->alloc_size;
   fprintf(f, " | size:allocated = %lu:%lu = %lu %% | used:allocated = %lu:%lu = %lu %%\n", m->size, m->alloc_size, avg, used, m->alloc_size, use_avg);
   fclose(f);
}

#endif


/*#undef MAP_KEY_COPY
#undef MAP_KEY_HASH
#undef MAP_KEY_EQUAL
#undef MAP_KEY_FREE
#undef MAP_VALUE_COPY
#undef MAP_VALUE_EQUAL
#undef MAP_VALUE_FREE

#undef MAP_SHOW_KEY
#undef MAP_SHOW_VALUE

#undef MAP_NAME
#undef MAP_KEY_TYPE
#undef MAP_VALUE_TYPE
#undef MAP_NOT_FOUND

#undef GLUE_TOGETHER
#undef MAKE_GENERIC_NAME
#undef MAP
#undef MAP_CELL
#undef PROTOTYPE
#undef TRAVERSER*/

