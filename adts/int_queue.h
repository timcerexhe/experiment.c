#ifndef INT_QUEUE_H
#define INT_QUEUE_H

#include <stdio.h>

#define QUEUE_NAME int
#define QUEUE_TYPE int

#define QUEUE_SHOW_VALUE(f,v) fprintf(f, "%d", v)

#include "adts/templates/queue.h"

#include "adts/glue/unglue_queue.h"

#endif

