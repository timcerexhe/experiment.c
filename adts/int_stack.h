#ifndef INT_STACK_H
#define INT_STACK_H

#include <stdio.h>

#define STACK_NAME int
#define STACK_TYPE int

#define STACK_SHOW_VALUE(f,v) fprintf(f, "%d", v)

#include "adts/templates/stack.h"

#include "adts/glue/unglue_stack.h"

#endif

