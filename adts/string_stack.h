#ifndef STRING_STACK_H
#define STRING_STACK_H

#include <stdio.h>
#include <string.h>

#define STACK_NAME string
#define STACK_TYPE char*

#define STACK_SHOW_VALUE(f,v) fprintf(f, "%s", v)

#define STACK_ELEMENT_COPY(v) strdup(v)
#define STACK_ELEMENT FREE(v) free(v) //we strdup'd, so free (don't MAGIC_ADT_FREE!)

#include "adts/templates/stack.h"

#include "adts/glue/unglue_stack.h"

#endif

