
#define GLUE_TOGETHER(A, B) A ## _ ## B
#define MAKE_GENERIC_NAME(A, B) GLUE_TOGETHER(A, B)

