
#ifndef AVL_TREE_TEMPLATE

   #undef AVL_TREE_NAME
   #undef AVL_TREE_TYPE
   #undef AVL_TREE_SHOW_VALUE
   #undef AVL_TREE_VALUE_COMPARE

   #undef AVL_TREE_ELEMENT_COPY
   #undef AVL_TREE_ELEMENT_FREE

   #undef AVL_TREE

   #undef PROTOTYPE
   #undef MAKE_GENERIC_NAME
   #undef GLUE_TOGETHER

#endif

