
#ifndef STACK_TEMPLATE

   #undef STACK_NAME
   #undef STACK_TYPE
   #undef STACK_SHOW_VALUE
   #undef STACK_VALUE_COMPARE

   #undef STACK_ELEMENT_FREE
   #undef STACK_ELEMENT_COPY

   #undef STACK

   #undef PROTOTYPE
   #undef MAKE_GENERIC_NAME
   #undef GLUE_TOGETHER

#endif

