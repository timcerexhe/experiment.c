
#ifndef QUEUE_TEMPLATE

   #undef QUEUE_NAME
   #undef QUEUE_TYPE
   #undef QUEUE_SHOW_VALUE
   #undef QUEUE_VALUE_COMPARE

   #undef QUEUE_ELEMENT_FREE
   #undef QUEUE_ELEMENT_COPY

   #undef QUEUE

   #undef PROTOTYPE
   #undef MAKE_GENERIC_NAME
   #undef GLUE_TOGETHER

#endif

