
#ifndef MAP_TEMPLATE

   #undef MAP_NAME
   #undef MAP_KEY_TYPE
   #undef MAP_VALUE_TYPE
   #undef MAP_NOT_FOUND

   #undef MAP_SHOW_VALUE

   #undef MAP_KEY_COPY
   #undef MAP_KEY_HASH
   #undef MAP_KEY_EQUAL
   #undef MAP_KEY_FREE
   #undef MAP_VALUE_COPY
   #undef MAP_VALUE_EQUAL
   #undef MAP_VALUE_FREE

   #undef MAP

   #undef PROTOTYPE
   #undef MAKE_GENERIC_NAME
   #undef GLUE_TOGETHER

#endif


