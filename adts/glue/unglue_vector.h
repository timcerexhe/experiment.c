
#ifndef VECTOR_TEMPLATE

   #undef VECTOR_NAME
   #undef VECTOR_TYPE
   #undef VECTOR_SHOW_VALUE
   #undef VECTOR_VALUE_COMPARE

   #undef VECTOR_ELEMENT_FREE
   #undef VECTOR_ELEMENT_COPY

   #undef VECTOR

   #undef PROTOTYPE
   #undef MAKE_GENERIC_NAME
   #undef GLUE_TOGETHER

#endif

