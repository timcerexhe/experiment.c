#ifndef STRING_QUEUE_H
#define STRING_QUEUE_H

#include <stdio.h>
#include <string.h>

#define QUEUE_NAME string
#define QUEUE_TYPE char*

#define QUEUE_SHOW_VALUE(f,v) fprintf(f, "%s", v)

#define QUEUE_ELEMENT_COPY(v) strdup(v)
#define QUEUE_ELEMENT_FREE(v) free(v) //we strdup'd --> so don't use MAGIC_ADT_FREE!

#include "adts/templates/queue.h"

#include "adts/glue/unglue_queue.h"

#endif

