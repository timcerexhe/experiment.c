#ifndef STRING_VECTOR_H
#define STRING_VECTOR_H

#include <stdio.h>
#include <string.h>

#define VECTOR_NAME string
#define VECTOR_TYPE char*

#define VECTOR_SHOW_VALUE(f,v) fprintf(f, "%s", v)

#define VECTOR_ELEMENT_COPY(v) strdup(v)
#define VECTOR_ELEMENT_FREE(v) free(v) //we strdup'd --> so don't use MAGIC_ADT_FREE!

#include "adts/templates/vector.h"

#include "adts/glue/unglue_vector.h"

#endif

