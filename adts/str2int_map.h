#ifndef STRING_2_INT_MAP_H
#define STRING_2_INT_MAP_H

#include <string.h>
#include "adts/hash_functions.h"

#define MAP_NAME str2int
#define MAP_KEY_TYPE char*
#define MAP_VALUE_TYPE int
#define MAP_NOT_FOUND -1

#define MAP_SHOW_KEY(file,key) fprintf(file, "%s", key)
#define MAP_SHOW_VALUE(file,value) fprintf(file, "%d", value)
#define MAP_KEY_COPY(key) strdup(key)
#define MAP_KEY_HASH(key) hash_murmurhash2(key)
#define MAP_KEY_EQUAL(k1, k2) (strcmp(k1, k2) == 0)
#define MAP_KEY_FREE(key) free(key) //we strdup'd --> so don't use MAGIC_ADT_FREE!

#include "adts/templates/map.h"

#include "adts/glue/unglue_map.h"

#endif

