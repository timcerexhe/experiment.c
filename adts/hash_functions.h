#ifndef HASH_FUNCTIONS
#define HASH_FUNCTIONS

__inline__ unsigned long hash_murmurhash2 (const char* const s);
__inline__ unsigned long hash_sdbm (const char* const s);
__inline__ unsigned long hash_in_four_char_blocks (const char* const s);

#endif

