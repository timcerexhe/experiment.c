#ifndef INT_VECTOR_H
#define INT_VECTOR_H

#include <stdio.h>

#define VECTOR_NAME int
#define VECTOR_TYPE int

#define VECTOR_SHOW_VALUE(f,v) fprintf(f, "%d", v)

#include "adts/templates/vector.h"

#include "adts/glue/unglue_vector.h"

#endif

