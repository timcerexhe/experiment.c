
#include "adts/hash_functions.h"
#include <string.h>

//
// consider "appropriating" some other hash functions from here:
// http://www.eternallyconfuzzled.com/tuts/algorithms/jsw_tut_hashing.aspx
//
// :)
//

/**
 * MurmurHash2
 *
 * NB. this implementation is slighly simplified -- we don't do crazy things
 * with trailing bits if the input is not divisible by four
 */
__inline__ unsigned long hash_murmurhash2 (const char* const s) {
   unsigned long m = 0x5bd1e995;
   unsigned long r = 24;
   unsigned long seed = 0x9747b28c;
   unsigned long len = strlen(s);
   unsigned long hash = seed ^ len;

   const char* c = s;
   unsigned long temp, i;
   while (*c != 0) {
      for (i = 0, temp = 0; *c != 0 && i < 4; ++i, ++c) {
         temp = *c + 256 * temp;
      }

      temp *= m;
      temp ^= (temp >> r);
      temp *= m;

      hash *= m;
      hash ^= temp;
   }

   hash ^= (hash >> 13);
   hash *= m;
   hash ^= (hash >> 15);

   return hash;
}

__inline__ unsigned long hash_sdbm (const char* const s) {
   unsigned long hash = 0;
   const char* c = s;
   while (*c != 0) {
      hash = *c + (hash << 6) + (hash << 16) - hash;
      ++c;
   }
   return hash;
}

__inline__ unsigned long hash_in_four_char_blocks (const char* const s) {
   unsigned long sum = 0;
   const char* c = s;
   unsigned long temp, i;
   while (*c != 0) {
      for (i = 0, temp = 0; *c != 0 && i < 4; ++i, ++c) {
         temp = *c + 256 * temp;
      }
      sum += temp;
   }
   return sum;
}


