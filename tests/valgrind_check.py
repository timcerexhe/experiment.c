#! /usr/bin/python

import sys, pexpect

USE_AT_EXIT    = 'in use at exit: ([\d,]+) bytes in ([\d,]+) blocks'
HEAP_USAGE     = 'total heap usage: ([\d,]+) allocs, ([\d,]+) frees, ([\d,]+) bytes allocated'
LEAK_CHECK_OK  = 'All heap blocks were freed -- no leaks are possible'
LEAK_CHECK_NOK = 'definitely lost: ([\d,]+) bytes in ([\d,]+) blocks'
ERROR_SUMMARY  = 'ERROR SUMMARY: ([\d,]+) errors from ([\d,]+) contexts'
SUPPRESSED     = '\(suppressed: ([\d,]+) from ([\d,]+)\)'

ANSI_CODES = {
   'red'   : '\033[31m',
   'yellow': '\033[93m',
   'green' : '\033[92m',
   'blue'  : '\033[94m',
   'reset' : '\033[0m',
}

def main(program):
   cmd = [ 'valgrind', '--leak-check=full', program ]
   #p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
   #p_out, p_err = p.communicate()

   errors = 0

   #run the command and skip to the valgrind summary
   child = pexpect.spawn(' '.join(cmd))
   child.expect('HEAP SUMMARY:')

   #how much memory was still in use at the end
   child.expect(USE_AT_EXIT)
   bytes_in_use  = to_number(child.match.group(1))
   blocks_in_use = to_number(child.match.group(2))

   if bytes_in_use == blocks_in_use == 0:
      pass
   else:
      print '%d Bytes (%d blocks) still in use at exit' % (bytes_in_use, blocks_in_use)
      errors += 1

   #collect some statistics
   child.expect(HEAP_USAGE)
   total_allocs = to_number(child.match.group(1))
   total_frees  = to_number(child.match.group(2))
   total_bytes  = to_number(child.match.group(3))
   print '%d Bytes used -- allocated %d / freed %d =' % (total_bytes, total_allocs, total_frees),
   cleanliness = 100.0*total_frees/total_allocs
   colour = 'green' if cleanliness == 100 else 'red'
   print_colourful('%f %% clean' % cleanliness, colour)

   #check the leak report
   if child.expect([LEAK_CHECK_OK, LEAK_CHECK_NOK]) == 0:
      pass
   else:
      bytes_lost  = to_number(child.match.group(1))
      blocks_lost = to_number(child.match.group(2))
      print 'definitely lost at least %d Bytes (%d blocks)' % (bytes_lost, blocks_lost)
      errors += 1

   #check the final error summary
   child.expect(ERROR_SUMMARY)
   total_errors   = to_number(child.match.group(1))
   total_contexts = to_number(child.match.group(2))

   if total_errors == total_contexts == 0:
      pass
   else:
      print 'valgrind found %d errors (%d contexts)' % (total_errors, total_contexts)
      errors += 1

   if child.expect([SUPPRESSED, pexpect.EOF]) == 0:
      suppressed_errors   = to_number(child.match.group(1))
      suppressed_contexts = to_number(child.match.group(2))
      if (suppressed_errors, suppressed_contexts) != (0,0):
         print_colourful('%d errors (from %d contexts) were suppressed :(' % (suppressed_errors, suppressed_contexts), 'yellow')
         #errors += 1 #turns out it really isn't my fault -- just dodgy system libraries

   print 'valgrind check is over!',

   if errors == 0:
      print_colourful('no errors found', 'green')
   else:
      print_colourful('some errors found', 'red')

   if errors == 0:
      print_colourful('YOU ARE AWESOME', 'blue')


def to_number(input):
   input = input.replace(',', '') #remove all the damn commas
   return float(input)


def print_colourful(msg, colour):
   start = None
   reset = None
   if colour in ANSI_CODES.keys():
      start = ANSI_CODES[colour]
      reset = ANSI_CODES['reset']
      print '%s%s%s' % (start, msg, reset)
   else:
      print msg


if __name__ == '__main__':
   if len(sys.argv) == 2:
      program = sys.argv[1]
      main(program)

   else:
      print 'usage: %s cmd' % __file__

