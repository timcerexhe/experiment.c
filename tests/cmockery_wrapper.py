#! /usr/bin/python

import sys, pexpect

ANSI_CODES = {
   'red'   : '\033[31m',
   'yellow': '\033[93m',
   'green' : '\033[92m',
   'blue'  : '\033[94m',
   'reset' : '\033[0m',
}

def print_colourful(msg, colour):
   start = None
   reset = None
   if colour in ANSI_CODES.keys():
      start = ANSI_CODES[colour]
      reset = ANSI_CODES['reset']
      print '%s%s%s' % (start, msg, reset)
   else:
      print msg


def main(script):
   tests = 0
   passes = 0
   fails = 0

   child = pexpect.spawn(script)
   i = child.expect(['(\S*): Starting test', 'All \d+ tests passed', 'Passed \d+ out of \d+ tests.', pexpect.EOF ])
   while i != 3:
      #print 'try ...' # % child.match.group(0)
      if i == 0:
         tests += 1
         #print 'found a test'
         #print 'before:', child.before
         #print 'after:', child.after
         #print 'match:', child.match.groups() if child.match else 'none :('
         test_name = child.match.group(1)

         length = max(50 - len(test_name), 0)
         sys.stdout.write('test %s %s ' % (test_name, length * '.'))

         if child.expect([ '%s: Test completed successfully.' % test_name, 'ERROR:']) == 0:
            passes += 1
            print_colourful('passed :)', 'green')

         else:
            child.expect('%s: Test failed.' % test_name)
            fails += 1
            print_colourful('failed :(', 'red')

         #print 'test is over'

      elif i == 1:
         #print "we're done :D"
         pass

      elif i == 2:
         #print "we're done (with some fails)"
         pass

      i = child.expect(['(\S*): Starting test', 'All \d+ tests passed', 'Passed \d+ out of \d+ tests.', pexpect.EOF ])


   #print 'tests over! %d passes + %d fails = %d tests' % (passes, fails, tests)
   assert passes + fails == tests
   if passes == tests and tests > 0:
      print_colourful('YOU ARE AWESOME!', 'blue')


if __name__ == '__main__':
   import sys
   if len(sys.argv) > 1:
      args = sys.argv[1:]
      main(' '.join(args))
   else:
      print 'usage: %s command [args ...]' % __file__

