#! /usr/bin/python

import sys, re

ANSI_CODES = {
   'red'   : '\033[31m',
   'bold red' : '\033[31m\033[1m',
   'yellow': '\033[93m',
   'green' : '\033[92m',
   'blue'  : '\033[94m',
   'reset' : '\033[0m',
}

DATA_POINT = re.compile('size:allocated = \d+:\d+ = (\d+) % \| used:allocated = \d+:\d+ = (\d+) %')


def print_colourful(msg, colour):
   if colour in ANSI_CODES.keys():
      start = ANSI_CODES[colour]
      reset = ANSI_CODES['reset']
      msg = '%s%s%s' % (start, msg, reset)

   sys.stdout.write(msg)

def main(logs):
   good = bad = ugly = contravenes_geneva_convention = 0
   for log in logs:
      #if len(logs) > 1:
      #   print ' === %s ===' % log

      f = file(log)
      for line in f.readlines():
         m = DATA_POINT.search(line)
         if m:
            aim = int(m.group(1))
            result = int(m.group(2))
            if abs(aim - result) <= 5:
               good += 1
            elif abs(aim - result) <= 10:
               bad += 1
            elif abs(aim - result) <= 20:
               ugly += 1
            else:
               contravenes_geneva_convention += 1

            #print 'aim for %d ... got %d' % (aim, result)

      f.close()

   count = good + bad + ugly + contravenes_geneva_convention
   print 'hash distribution summary from %d tests:' % count

   if good > 0:
      print ' * ',
      print_colourful('%d <= 5 %%' % good, 'green')
      print ' of optimal'

   if bad > 0:
      print ' * ',
      print_colourful('%d <= 10 %%' % bad, 'yellow')
      print ' of optimal'

   if ugly > 0:
      print ' * ',
      print_colourful('%d > 10 %%' % ugly, 'red')
      print ' of optimal'

   if contravenes_geneva_convention > 0:
      print ' * ',
      print_colourful('%d > 20 %%' % contravenes_geneva_convention, 'bold red')
      print ' of optimal'

   if good == count and count > 0:
      print_colourful('YOU ARE AWESOME!', 'blue')
      print


if __name__ == '__main__':
   import sys
   if len(sys.argv) > 1:
      args = sys.argv[1:]
      main(args)
   else:
      print 'usage: %s log_file [log_file ...]' % __file__

