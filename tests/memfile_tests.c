#include <malloc.h>

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <google/cmockery.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "file/memfile.h"


void test_zero_empty (void** state) {
   memfile mf = memfile_create (0);
   memfile_destroy (mf);
}

void test_small_empty (void** state) {
   memfile mf = memfile_create (10);
   memfile_destroy (mf);
}

void test_write_no_read (void** state) {
   memfile mf = memfile_create (100);
   FILE* w = memfile_write (mf);
   fprintf (w, "hello memfile\n%d :)", 3);
   fclose (w);
   memfile_destroy (mf);
}

void test_write_and_read (void** state) {
   memfile mf = memfile_create (100);

   FILE* w = memfile_write (mf);
   printf ("write something\n");
   fprintf (w, "hello memfile\n%d :)\n", 5);
   printf ("written!\n");
   fclose (w);

   FILE* r = memfile_read (mf);
   char str[100];
   printf("read\n");
   assert_true (fgets (str, 100, r));
   assert_int_equal (strcmp (str, "hello memfile\n"), 0);
   printf("read!\n");

   printf("read again\n");
   assert_true (fgets (str, 100, r));
   assert_int_equal (strcmp (str, "5 :)\n"), 0);
   printf("read again!\n");
   fclose (r);

   memfile_destroy (mf);
}

int main () {
   const UnitTest tests[] = {
      unit_test (test_zero_empty),
      unit_test (test_small_empty),
      unit_test (test_write_no_read),
      unit_test (test_write_and_read),
   };

   return run_tests (tests);
}


