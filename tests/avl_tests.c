#include <malloc.h>

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <google/cmockery.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "adts/int_avl_tree.h"
#include "file/memfile.h"


void test_empty (void** state) {
   int_avl_tree t = int_avl_tree_create ();
   //int_avl_tree_insert (t, 3);
   assert_int_equal (int_avl_tree_size (t), 0);
   //assert_int_equal (strcmp(int_avl_tree_show (t), EMPTY_TREE), 0);
   int_avl_tree_show(t);
   int_avl_tree_destroy (t);
}

void test_insert_rm(void** state) {
   int_avl_tree t = int_avl_tree_create ();
   assert_int_equal (int_avl_tree_size (t), 0);

   int num = 34612;
   assert_false (int_avl_tree_find (t, num));
   int_avl_tree_insert (t, num);
   assert_true (int_avl_tree_find (t, num));

   assert_int_equal (int_avl_tree_size (t), 1);
   assert_false (int_avl_tree_remove (t, num + 10));
   assert_int_equal (int_avl_tree_size (t), 1);

   assert_true (int_avl_tree_remove (t, num));
   assert_int_equal (int_avl_tree_size (t), 0);

   int_avl_tree_destroy (t);
}

void test_show_a_few(void** state) {
   int_avl_tree t = int_avl_tree_create ();
   assert_int_equal (int_avl_tree_size (t), 0);

   int first_number = -10;
   int last_number  =  10;

   int i;
   for (i = first_number; i <= last_number; ++i) {
      assert_int_equal (int_avl_tree_size (t), i - first_number);
      assert_false (int_avl_tree_find (t, i));
      int_avl_tree_insert (t, i);
      assert_true (int_avl_tree_find (t, i));
      assert_int_equal (int_avl_tree_size (t), i - first_number + 1);
   }

   int_avl_tree_show (t);

   int_avl_tree_destroy (t);
}

void test_insert_many(void** state) {
   int_avl_tree t = int_avl_tree_create ();
   assert_int_equal (int_avl_tree_size (t), 0);

   int first_number = -100;
   int last_number  =  100;

   int i;
   for (i = first_number; i <= last_number; ++i) {
      assert_int_equal (int_avl_tree_size (t), i - first_number);
      assert_false (int_avl_tree_find (t, i));
      int_avl_tree_insert (t, i);
      assert_true (int_avl_tree_find (t, i));
      assert_int_equal (int_avl_tree_size (t), i - first_number + 1);
   }

   int_avl_tree_destroy (t);
}

void test_remove(void** state) {
   int_avl_tree t = int_avl_tree_create ();

   assert_int_equal (int_avl_tree_size (t), 0);

   int bounds = 100;

   int i;
   for (i = 1; i <= bounds; ++i) { //start at 1 to avoid 0 and -0
      assert_int_equal (int_avl_tree_size (t), 2 * (i - 1));
      assert_false (int_avl_tree_find (t,  i));
      int_avl_tree_insert (t, i);
      assert_true  (int_avl_tree_find (t,  i));
      assert_false (int_avl_tree_find (t, -i));
      int_avl_tree_insert (t, -i);
      assert_true  (int_avl_tree_find (t, -i));
   }

   assert_int_equal (int_avl_tree_size (t), 2 * bounds);
   assert_false (int_avl_tree_remove (t, bounds + 20));

   int size = 2 * bounds;
   assert_int_equal (int_avl_tree_size (t), size);

   for (i = bounds / 2; i <= bounds; i += 3) {
      assert_int_equal (int_avl_tree_size (t), size);
      assert_true (int_avl_tree_remove (t, i));
      --size;
      assert_int_equal (int_avl_tree_size (t), size);
   }

   for (i = bounds / 2; i <= bounds; i += 3) {
      assert_false (int_avl_tree_remove (t, i));
      assert_int_equal (int_avl_tree_size (t), size);
   }

   int_avl_tree_destroy (t);
}

void test_show(void** state) {
   int_avl_tree t = int_avl_tree_create ();

   memfile mf;
   FILE* w;

   //empty tree
   mf = memfile_create (1024);
   w = memfile_write (mf);
   int_avl_tree_fshow (t, w);
   assert_int_equal (strcmp (memfile_string (mf), "<empty tree>\n"), 0);
   fclose (w);
   memfile_destroy (mf);

   int_avl_tree_insert (t, 3);
   int_avl_tree_insert (t, -1);

   mf = memfile_create (1024);
   w = memfile_write (mf);
   int_avl_tree_fshow (t, w);
   assert_int_equal (strcmp (memfile_string (mf), "3\n   -1\n"), 0);
   fclose (w);
   memfile_destroy (mf);

   int_avl_tree_destroy (t);
}

int main () {
   const UnitTest tests[] = {
      unit_test (test_empty),
      unit_test (test_insert_rm),
      unit_test (test_show_a_few),
      unit_test (test_insert_many),
      unit_test (test_remove),
      unit_test (test_show),
   };

   return run_tests (tests);
}


