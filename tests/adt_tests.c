#include <malloc.h>

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <google/cmockery.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "adts/int_vector.h"
#include "adts/string_vector.h"

#include "adts/int_stack.h"
#include "adts/string_stack.h"

#include "adts/int_queue.h"
#include "adts/string_queue.h"

#include "adts/str2int_map.h"

void test_int_vector (void** state) {
   int_vector iv = int_vector_create ();
   int_vector_push_back (iv, 3);
   assert_int_equal (int_vector_remove (iv, 0), 3);
   int_vector_destroy (iv);
}

void test_string_vector (void** state) {
   string_vector sv = string_vector_create ();
   assert_int_equal (string_vector_size (sv), 0);

   string_vector_push_back (sv, "vector 1");
   assert_int_equal (string_vector_size (sv), 1);
   assert_int_equal (strcmp (string_vector_at (sv, 0), "vector 1"), 0);

   char vector1[] = "vector 1";
   assert_int_equal (strcmp (string_vector_remove (sv, 0), vector1), 0);
   assert_int_equal (string_vector_size (sv), 0);

   int first_number = 0;
   int last_number = 10;

   int i;
   size_t count = 0;
   for (i = first_number; i <= last_number; ++i) {
      char str[15];
      snprintf(str, 15, "vector %d", i);
      //if (i != 0) {
         assert_int_equal (string_vector_size (sv), count);
         string_vector_push_back (sv, str);
         ++count;
      //}
   }

   for (i = last_number; i >= first_number; --i) {
      char str[15];
      snprintf(str, 15, "vector %d", i);
      //if (i != 0) {
         assert_int_equal (string_vector_size (sv), count);
         assert_int_equal (strcmp (string_vector_at (sv, i), str), 0);
      //} 
   }

   for (i = last_number; i >= 0; i -= 2) {
      char str[15];
      snprintf(str, 15, "vector %d", i);
      assert_int_equal (strcmp (string_vector_remove (sv, i), str), 0);
      --count;
   }

   int expected = (last_number-first_number) / 2;
   for (i = 0; i < expected; ++i) {
      char different_str[15];
      snprintf(different_str, 15, "vector %d", 2*i+1);
      //printf("%d: next %s expecting %s\n", i, string_vector_at(sv, 0), different_str);
      assert_int_equal (strcmp (string_vector_remove (sv, 0), different_str), 0);
      --count;
   }

   assert_int_equal (string_vector_size (sv), 0);

   string_vector_destroy (sv);
}

void test_int_stack (void** state) {
   int_stack s = int_stack_create ();

   assert_int_equal (int_stack_size (s), 0);

   int_stack_push (s, 10);
   assert_int_equal (int_stack_size (s), 1);
   assert_int_equal (int_stack_top (s), 10);
   assert_int_equal (int_stack_pop (s), 10);
   assert_int_equal (int_stack_size (s), 0);

   int first_number = -100;
   int last_number = 100;

   int i;
   size_t count = 0;
   for (i = first_number; i != last_number + 1; ++i) {
      if (i != 0) {
         assert_int_equal (int_stack_size (s), count);
         int_stack_push (s, i);
         ++count;
      }
   }

   for (i = last_number; i != first_number - 1; --i) {
      if (i != 0) {
         assert_int_equal (int_stack_size (s), count);
         assert_int_equal (int_stack_top (s), i);
         assert_int_equal (int_stack_pop (s), i);
         --count;
      }
   }

   assert_int_equal (int_stack_size (s), 0);

   int_stack_destroy (s);
}

void test_string_stack (void** state) {
   string_stack s = string_stack_create ();

   assert_int_equal (string_stack_size (s), 0);

   string_stack_push (s, "stack 1");
   assert_int_equal (string_stack_size (s), 1);
   assert_int_equal (strcmp (string_stack_top (s), "stack 1"), 0);
   assert_int_equal (strcmp (string_stack_pop (s), "stack 1"), 0);
   assert_int_equal (string_stack_size (s), 0);

   int first_number = -100;
   int last_number = 100;

   int i;
   size_t count = 0;
   for (i = first_number; i != last_number + 1; ++i) {
      char str[10];
      snprintf(str, 10, "stack %d", i);
      //if (i != 0) {
         assert_int_equal (string_stack_size (s), count);
         string_stack_push (s, str);
         ++count;
      //}
   }

   for (i = last_number; i != first_number - 1; --i) {
      char str[10];
      snprintf(str, 10, "stack %d", i);
      //if (i != 0) {
         assert_int_equal (string_stack_size (s), count);
         assert_int_equal (strcmp (string_stack_top (s), str), 0);
         assert_int_equal (strcmp (string_stack_pop (s), str), 0);
         --count;
      //}
   }

   assert_int_equal (string_stack_size (s), 0);

   string_stack_destroy (s);
}

void test_int_queue (void** state) {
   int_queue q = int_queue_create ();

   assert_int_equal (int_queue_size (q), 0);

   int_queue_push (q, 10);
   assert_int_equal (int_queue_size (q), 1);
   assert_int_equal (int_queue_top (q), 10);
   assert_int_equal (int_queue_pop (q), 10);
   assert_int_equal (int_queue_size (q), 0);

   int first_number = -100;
   int last_number = 100;

   int i;
   size_t count = 0;
   for (i = first_number; i != last_number + 1; ++i) {
      if (i != 0) {
         assert_int_equal (int_queue_size (q), count);
         int_queue_push (q, i);
         ++count;
      }
   }

   for (i = first_number; i != last_number + 1; ++i) {
      if (i != 0) {
         assert_int_equal (int_queue_size (q), count);
         assert_int_equal (int_queue_top (q), i);
         assert_int_equal (int_queue_pop (q), i);
         --count;
      }
   }

   assert_int_equal (int_queue_size (q), 0);

   int_queue_destroy (q);
}

void test_string_queue (void** state) {
   string_queue q = string_queue_create ();

   assert_int_equal (string_queue_size (q), 0);

   string_queue_push (q, "queue 1");
   assert_int_equal (string_queue_size (q), 1);
   assert_int_equal (strcmp (string_queue_top (q), "queue 1"), 0);
   assert_int_equal (strcmp (string_queue_pop (q), "queue 1"), 0);
   assert_int_equal (string_queue_size (q), 0);

   int first_number = -100;
   int last_number = 100;

   int i;
   size_t count = 0;
   for (i = first_number; i != last_number + 1; ++i) {
      char str[10];
      if (i != 0) {
         snprintf(str, 10, "queue %d", i);
         assert_int_equal (string_queue_size (q), count);
         string_queue_push (q, str);
         ++count;
      }
   }

   for (i = first_number; i != last_number + 1; ++i) {
      char str[10];
      if (i != 0) {
         snprintf(str, 10, "queue %d", i);
         assert_int_equal (string_queue_size (q), count);
         assert_int_equal (strcmp (string_queue_top (q), str), 0);
         assert_int_equal (strcmp (string_queue_pop (q), str), 0);
         --count;
      }
   }

   assert_int_equal (string_queue_size (q), 0);

   string_queue_destroy (q);
}

void test_string_int_map (void** state) {
   str2int_map m = str2int_map_create ();
   assert_int_equal (str2int_map_size (m), 0);

   assert_true (!str2int_map_has_key (m, "some_key"));
   str2int_map_remove (m, "some_key"); //try removing it anyway!

   assert_int_equal (str2int_map_size (m), 0);

   str2int_map_insert (m, "some_key", 10);
   assert_int_equal (str2int_map_size (m), 1);
   assert_true (str2int_map_has_key (m, "some_key"));
   assert_int_equal (str2int_map_get (m, "some_key"), 10);

   //repeat these tests with a different string to check it is strcmp'ing
   char* key = strdup("some_key");
   assert_int_equal (str2int_map_size (m), 1);
   assert_true (str2int_map_has_key (m, key));
   assert_int_equal (str2int_map_get (m, key), 10);
   free (key);

   str2int_map_destroy (m);
}

#define BIG_NUMBER 100
#define NUMBER_PRINT_SIZE 7

void test_big_string_int_map (void** state) {
   str2int_map m = str2int_map_create ();

   int i;
   char s[NUMBER_PRINT_SIZE];
   for (i = 0; i < BIG_NUMBER; ++i) {
      snprintf (s, NUMBER_PRINT_SIZE, "%d", i);
      assert_int_equal (str2int_map_size (m), i);
      assert_true (!str2int_map_has_key (m, s));
      str2int_map_insert (m, s, i);
   }

   for (i = 0; i < BIG_NUMBER; ++i) {
      snprintf (s, NUMBER_PRINT_SIZE, "%d", i);
      assert_true (str2int_map_has_key (m, s));
      assert_int_equal (str2int_map_size (m), BIG_NUMBER);
      assert_int_equal (str2int_map_get (m, s), i);
   }

   str2int_map_destroy (m);
}

/*void test_fsymbol2fcode_map (void** state) {
   fsymbol2fcode_map m = fsymbol2fcode_map_create ();

   int i;
   char s[NUMBER_PRINT_SIZE];
   for (i = 1; i <= BIG_NUMBER; ++i) {
      snprintf (s, NUMBER_PRINT_SIZE, "%d", i);
      struct function_symbol fs;
      fs.name = s;
      fs.arity = i;

      assert_int_equal (fsymbol2fcode_map_size (m), i-1);
      assert_true (!fsymbol2fcode_map_has_key (m, fs));
      fsymbol2fcode_map_insert (m, fs, i);
   }

   for (i = 1; i <= BIG_NUMBER; ++i) {
      snprintf (s, NUMBER_PRINT_SIZE, "%d", i);
      struct function_symbol fs;
      fs.name = s;
      fs.arity = i;

      assert_true (fsymbol2fcode_map_has_key (m, fs));
      assert_int_equal (fsymbol2fcode_map_size (m), BIG_NUMBER);
      assert_int_equal (fsymbol2fcode_map_get (m, fs), i);
   }

   fsymbol2fcode_map_destroy (m);
}

void test_fcode2fsymbol_map (void** state) {
   fcode2fsymbol_map m = fcode2fsymbol_map_create ();

   FunctionCode i;
   char s[NUMBER_PRINT_SIZE];
   for (i = 1; i <= BIG_NUMBER; ++i) {
      snprintf (s, NUMBER_PRINT_SIZE, "%ld", i);
      struct function_symbol fs;
      fs.name = s;
      fs.arity = i;

      assert_int_equal (fcode2fsymbol_map_size (m), i-1);
      assert_true (!fcode2fsymbol_map_has_key (m, i));
      fcode2fsymbol_map_insert (m, i, fs);
   }

   for (i = 1; i <= BIG_NUMBER; ++i) {
      snprintf (s, NUMBER_PRINT_SIZE, "%ld", i);

      assert_true (fcode2fsymbol_map_has_key (m, i));
      assert_int_equal (fcode2fsymbol_map_size (m), BIG_NUMBER);
      struct function_symbol ret = fcode2fsymbol_map_get (m, i);
      assert_true (strcmp (s, ret.name) == 0);
      assert_int_equal (ret.arity, i);
   }

   fcode2fsymbol_map_destroy (m);
}*/

int main () {
   const UnitTest tests[] = {
      unit_test (test_int_vector),
      unit_test (test_string_vector),
      unit_test (test_int_stack),
      unit_test (test_string_stack),
      unit_test (test_int_queue),
      unit_test (test_string_queue),
      unit_test (test_string_int_map),
      unit_test (test_big_string_int_map),
   };

   return run_tests (tests);
}

