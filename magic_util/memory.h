#ifndef MAGIC_ADT_MEMORY_H
#define MAGIC_ADT_MEMORY_H

//#define MAGIC_ADT_MEMORY_OVERRIDE 1

#define MAGIC_ADT_CALLOC(num, size) calloc(num, size)

#ifdef MAGIC_ADT_MEMORY_OVERRIDE

   #include <sys/types.h>

   void* debug_magic_adt_malloc(size_t size, const char* const file, int line);
   void debug_magic_adt_free(void* ptr, const char* const file, int line);

   #define MAGIC_ADT_MALLOC(size) debug_magic_adt_malloc(size, __FILE__, __LINE__)
   #define MAGIC_ADT_FREE(ptr) debug_magic_adt_free(ptr, __FILE__, __LINE__)

#else

   #define MAGIC_ADT_MALLOC(size) malloc(size)
   #define MAGIC_ADT_FREE(ptr) free(ptr)

#endif

#endif

