#ifndef MAGIC_ADT_ERROR
#define MAGIC_ADT_ERROR

#include <stdio.h>
#include <assert.h>

#define ASSERT(condition, msg, ...) if (!(condition)) { fprintf(stderr, msg, ##__VA_ARGS__); assert(condition); }

#endif
