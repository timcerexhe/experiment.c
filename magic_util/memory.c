#include <malloc.h>
#include <stdio.h>
#include "adts/templates/memory.h"

void* debug_magic_adt_malloc(size_t size, const char* const file, int line) {
   void* ptr = malloc(size);
   printf("malloc %zd bytes @ %p: %s %d\n", size, ptr, file, line);
   return ptr;
}

void debug_magic_adt_free(void* ptr, const char* const file, int line) {
   printf("free %p: %s %d\n", ptr, file, line);
   free(ptr);
}


