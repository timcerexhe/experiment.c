#ifndef MEMFILE_H
#define MEMFILE_H

#include <stdio.h>

typedef struct memfile* memfile;

memfile memfile_create (size_t max_size);
FILE* memfile_read (memfile mf);
FILE* memfile_write (memfile mf);
const char* memfile_string (memfile mf);
void memfile_destroy (memfile mf);

#endif
