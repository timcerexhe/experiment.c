#include <stdlib.h>

#include "magic_util/memory.h"
#include "file/memfile.h"


struct memfile {
   char* buffer;
   size_t buffer_size;
};


memfile memfile_create (size_t max_size) {
   memfile mf = MAGIC_ADT_MALLOC (sizeof(*mf));
   mf->buffer = MAGIC_ADT_MALLOC (max_size * sizeof(char));
   mf->buffer_size = max_size;
   return mf;
}

FILE* memfile_read (memfile mf) {
   return fmemopen (mf->buffer, mf->buffer_size, "r");
}

FILE* memfile_write (memfile mf) {
   return fmemopen (mf->buffer, mf->buffer_size, "w");
}

const char* memfile_string (memfile mf) {
   return mf->buffer;
}

void memfile_destroy (memfile mf) {
   MAGIC_ADT_FREE (mf->buffer);
   MAGIC_ADT_FREE (mf);
}


